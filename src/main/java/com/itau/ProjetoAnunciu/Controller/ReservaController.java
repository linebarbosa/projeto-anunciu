package com.itau.ProjetoAnunciu.Controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.ProjetoAnunciu.Model.Anuncio;
import com.itau.ProjetoAnunciu.Model.Reserva;
import com.itau.ProjetoAnunciu.Model.StatusAnuncio;
import com.itau.ProjetoAnunciu.Repository.AnuncioRepository;
import com.itau.ProjetoAnunciu.Repository.ReservaRepository;
import com.itau.ProjetoAnunciu.Service.TokenService;
import com.itau.ProjetoAnunciu.Service.UsuarioSessao;

@Controller
public class ReservaController {

	@Autowired
	ReservaRepository reservaRepository;
	
	@Autowired
	AnuncioRepository anuncioRepository;

	@Autowired
	TokenService tokenService;
	
	@Autowired
	UsuarioSessao sessao;

	@RequestMapping(path="/reserva/cadastrar", method=RequestMethod.POST)
	@ResponseBody
	public Reserva inserirReserva(HttpServletRequest request, @RequestBody Reserva reserva) {
		
		reserva.setUsuario(sessao.verificarSessao(request.getHeader("Authorization")));
		
		Anuncio anuncio = anuncioRepository.findById(reserva.getAnuncio().getId());
		
		anuncio.setStatus(StatusAnuncio.Reservado.toString());
		
		anuncioRepository.save(anuncio);
		return reservaRepository.save(reserva);
	}

	@RequestMapping(path="/reservas", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Reserva> getVariosAnuncios() {		
		return reservaRepository.findAll();
	}

	@RequestMapping(path="/reserva/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Reserva getReservaById(@PathVariable int id) {		
		return reservaRepository.findById(id);
	}
	
	@RequestMapping(path="/reservas/usuario", method=RequestMethod.GET)
	@ResponseBody
	public Set<Reserva> getReservasByUsuario(HttpServletRequest request) {	

		String cpf = sessao.verificarSessao(request.getHeader("Authorization")).getCpf();
		
		return reservaRepository.findAllByUsuarioCpf(cpf);
	}
	
	@RequestMapping(path="/reserva/confirmar", method=RequestMethod.POST)
	@ResponseBody
	public boolean confirmarReserva(HttpServletRequest request, @RequestBody Reserva reserva) {
		
		reserva.setUsuario(sessao.verificarSessao(request.getHeader("Authorization")));
		
		Anuncio anuncio = anuncioRepository.findById(reserva.getAnuncio().getId());
		
		anuncio.setStatus(StatusAnuncio.Vendido.toString());
		
		anuncioRepository.save(anuncio);
		return true;
	}
	
	@RequestMapping(path="/reserva/cancelar", method=RequestMethod.POST)
	@ResponseBody
	public Reserva cancelarReserva(HttpServletRequest request, @RequestBody Reserva reserva) {		
		reserva.setUsuario(sessao.verificarSessao(request.getHeader("Authorization")));
		
		Anuncio anuncio = anuncioRepository.findById(reserva.getAnuncio().getId());		
		anuncio.setStatus(StatusAnuncio.Ativo.toString());		
		anuncioRepository.save(anuncio);
		
		Reserva reservaCancelada = reservaRepository.findById(reserva.getId());		
		reservaCancelada.setStatus(false);		
		return reservaRepository.save(reservaCancelada);
	}
}