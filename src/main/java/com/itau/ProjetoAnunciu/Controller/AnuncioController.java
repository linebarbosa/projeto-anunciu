  package com.itau.ProjetoAnunciu.Controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.ProjetoAnunciu.Model.Anuncio;
import com.itau.ProjetoAnunciu.Model.Reserva;
import com.itau.ProjetoAnunciu.Model.StatusAnuncio;
import com.itau.ProjetoAnunciu.Model.Usuario;
import com.itau.ProjetoAnunciu.Repository.AnuncioRepository;
import com.itau.ProjetoAnunciu.Repository.ReservaRepository;
import com.itau.ProjetoAnunciu.Repository.UsuarioRepository;
import com.itau.ProjetoAnunciu.Service.TokenService;
import com.itau.ProjetoAnunciu.Service.UsuarioSessao;

@Controller
public class AnuncioController {
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	ReservaRepository reservaRepository;
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	UsuarioSessao sessao;
	
	
	@RequestMapping(path="/anuncio/cadastrar", method=RequestMethod.POST)
	@ResponseBody
	public Anuncio cadastrarAnuncio(HttpServletRequest request, @RequestBody Anuncio anuncio) {
		
		anuncio.setUsuario(sessao.verificarSessao(request.getHeader("Authorization")));
		
		return anuncioRepository.save(anuncio);
	}
	
	
	@RequestMapping(path="/anuncio/consultar", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getAllVariosAnuncios() {	
		return anuncioRepository.findAllByStatus(StatusAnuncio.Ativo.toString());
	}
	
	@RequestMapping(path="/anuncio/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Anuncio getAnuncioById(@PathVariable int id) {		
		return anuncioRepository.findById(id);
	}
	
	@RequestMapping(path="/anuncios/usuario", method=RequestMethod.GET)
	@ResponseBody
	public Set<Anuncio> getAnunciosByUsuario(HttpServletRequest request) {	

		String cpf = sessao.verificarSessao(request.getHeader("Authorization")).getCpf();
		
		return anuncioRepository.findAllByUsuarioCpf(cpf);
	}
	
	@RequestMapping(path="/anuncio/cancelar", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> cancelarAnuncio(HttpServletRequest request, @RequestBody Anuncio anuncio) {
		
		Usuario usuario = sessao.verificarSessao(request.getHeader("Authorization"));
		Anuncio anuncioInteiro = anuncioRepository.findById(anuncio.getId());
		
		if (usuario.getCpf().toString() == anuncioInteiro.getUsuario().getCpf().toString()) {
			
			anuncioInteiro.setStatus(StatusAnuncio.Inativo.toString());
			anuncioRepository.save(anuncioInteiro);
			
			return new ResponseEntity<Anuncio> (anuncioInteiro, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();

	}
	
	@RequestMapping(path="/anuncio/cancelarReserva", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> cancelarReservaAnuncio(HttpServletRequest request, @RequestBody Anuncio anuncio) {
		
		Usuario usuario = sessao.verificarSessao(request.getHeader("Authorization"));
		
		Anuncio anuncioInteiro = anuncioRepository.findById(anuncio.getId());
		
		if (usuario.getCpf().toString() == anuncioInteiro.getUsuario().getCpf().toString()) {
			
			Reserva reserva = reservaRepository.findByAnuncioAndStatus(anuncioInteiro, true);
			
			reserva.setStatus(false);
			reservaRepository.save(reserva);
			
			anuncioInteiro.setStatus(StatusAnuncio.Ativo.toString());
			anuncioRepository.save(anuncioInteiro);
			
			return new ResponseEntity<Anuncio> (anuncioInteiro, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();

	}
	
	@RequestMapping(path="/anuncio/confirmarReserva", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> confirmarReservaAnuncio(HttpServletRequest request, @RequestBody Anuncio anuncio) {
		
		Usuario usuario = sessao.verificarSessao(request.getHeader("Authorization"));
		
		Anuncio anuncioInteiro = anuncioRepository.findById(anuncio.getId());
		
		if (usuario.getCpf().toString() == anuncioInteiro.getUsuario().getCpf().toString()) {
			
			anuncioInteiro.setStatus(StatusAnuncio.Vendido.toString());
			anuncioRepository.save(anuncioInteiro);
			
			return new ResponseEntity<Anuncio> (anuncioInteiro, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();

	}
}
  