package com.itau.ProjetoAnunciu.Controller;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.ProjetoAnunciu.Model.Usuario;
import com.itau.ProjetoAnunciu.Repository.UsuarioRepository;
import com.itau.ProjetoAnunciu.Service.PasswordService;
import com.itau.ProjetoAnunciu.Service.TokenService;
import com.itau.ProjetoAnunciu.Service.UsuarioSessao;

@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PasswordService passwordService;

	@Autowired
	UsuarioSessao usuarioSessao;

	TokenService tokenService = new TokenService();

	//Insert do usuario - cadastro
	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}

	@RequestMapping(path="/usuario/alteracao", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> alterarUsuario(@RequestBody Usuario usuario, HttpServletRequest request) {
		Usuario usuarioLogado = usuarioSessao.verificarSessao(request.getHeader("Authorization"));
		
		if (usuarioLogado == null) {
			return ResponseEntity.badRequest().build();
		}
		
//		Field[] campos = Usuario.getDeclaredFields();
//		
//		for (Field campo: campos) {
//			if (usuario.campo.getNome() != null) {
//				usuarioLogado.campo.getNome() = usuario.campo.getNome();
//			}
//		}

		if (usuario.getSenha() != "") {
			String hash = passwordService.encode(usuario.getSenha());
			usuario.setSenha(hash);
		} else {
			usuario.setSenha(usuarioLogado.getSenha());
		}
		
		usuarioRepository.save(usuario);

		return ResponseEntity.ok(usuario);
	}

	@RequestMapping(path="/usuario/consulta", method=RequestMethod.GET)
	@ResponseBody
	public Usuario getUsuario(HttpServletRequest request) {
		String cpf = usuarioSessao.verificarSessao(request.getHeader("Authorization")).getCpf();
		return usuarioRepository.findByCpf(cpf);
	}

	@RequestMapping(path="/usuario/login", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> logar (@RequestBody Usuario usuario){
		Usuario usuarioBanco = usuarioRepository.findByCpf(usuario.getCpf());
		boolean senhaOk = passwordService.verificar(usuario.getSenha(), usuarioBanco.getSenha());

		if (senhaOk) {
			String token = tokenService.gerar(usuarioBanco.getCpf());
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			return new ResponseEntity<Usuario> (usuarioBanco, headers, HttpStatus.OK);
		}

		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/usuario/check", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> verificar (HttpServletRequest request){
		Usuario usuarioLogado = usuarioSessao.verificarSessao(request.getHeader("Authorization"));

		if (usuarioLogado != null) {
			return ResponseEntity.ok(usuarioLogado);
		}

		return ResponseEntity.badRequest().build();
	}
}
