package com.itau.ProjetoAnunciu.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.ProjetoAnunciu.Model.Usuario;
import com.itau.ProjetoAnunciu.Repository.UsuarioRepository;

@Service
public class UsuarioSessao {

	@Autowired
	TokenService tokenService;

	@Autowired
	UsuarioRepository usuarioRepository;

	public Usuario verificarSessao(String bearer) {
		String token = bearer.replace("Bearer ", "");

		if (tokenService.verificar(token) != null) {
			Usuario usuario = usuarioRepository.findByCpf(tokenService.verificar(token));
			return usuario;
		}
		
		return null;

	}

}
