package com.itau.ProjetoAnunciu.Repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.ProjetoAnunciu.Model.Anuncio;
import com.itau.ProjetoAnunciu.Model.Reserva;

public interface ReservaRepository extends CrudRepository<Reserva, Integer>{
	
	Reserva findById(int id);
	
	Set<Reserva> findAllByUsuarioCpf(String usuarioCpf);
	
	Reserva findByAnuncioAndStatus(Anuncio anuncio, boolean status);
	
//	Set<Reserva> findByAnuncio(int idAnuncio);

}
