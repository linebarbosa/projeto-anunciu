  package com.itau.ProjetoAnunciu.Repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.ProjetoAnunciu.Model.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, Integer>{
	
	Anuncio findById(int id);
	
	Set<Anuncio> findAllByUsuarioCpf(String usuarioCpf);
	
	Set<Anuncio> findAllByStatus(String status);

}