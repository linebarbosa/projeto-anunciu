package com.itau.ProjetoAnunciu.Repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.ProjetoAnunciu.Model.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Integer>{

}
