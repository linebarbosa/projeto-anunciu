package com.itau.ProjetoAnunciu.Repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.ProjetoAnunciu.Model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	Usuario findByCpf(String cpf);
}
