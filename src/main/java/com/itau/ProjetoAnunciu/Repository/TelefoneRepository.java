package com.itau.ProjetoAnunciu.Repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.ProjetoAnunciu.Model.Telefone;

public interface TelefoneRepository extends CrudRepository<Telefone, Integer> {

}
