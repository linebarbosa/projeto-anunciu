package com.itau.ProjetoAnunciu.Model;

public enum StatusAnuncio {
	Ativo("Ativo"),
	Inativo("Inativo"),
	Reservado("Reservado"),
	Vendido("Vendido");

	String status;
	
	StatusAnuncio (String status){
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

}
